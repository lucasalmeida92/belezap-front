import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from '../../containers/App/GlobalStyles';

const Wrapper = styled.div`
  display: flex;
  position: relative;
  margin-bottom: 20px;
  ${({ hasLabel }) => hasLabel && `
    margin-top: 65px;
  `}
`;

const Input = styled.input`
  outline: none;
  max-width: 100%;
  width: 100%;
  height: ${({big}) => (big
    ? '56px'
    : '48px')};
  box-sizing: border-box;
  padding: 0 20px;
  ${({ icon }) => icon && `
    padding-left: 50px;
  `}
  display: block;
  opacity: 1;
  border-radius: 4px;
  border: none;
  box-shadow: 0 1px 4px 0px rgba(0, 0, 0, 0.12);
  background-color: #fff;
  font-weight: 500;
  font-size: ${({big}) => (big
    ? '18px'
    : '16px')};
  color: #808080;
  line-height: ${({big}) => (big
    ? '56px'
    : '48px')};
`;

const Icon = styled.i`
  position: absolute;
  left: 17px;
  display: flex;
  align-items: center;
  height: 100%;
  color: #b0b0b0;
`;

const Label = styled.label`
  position: absolute;
  top: 0;
  left: 0;
  transform: translateY(calc(-100% - 15px));
  background: ${colors.primaryColor};
  border-radius: 4px;
  padding: 5px 10px;
  line-height: 22px;
  color: #fff;
`;

const TextField = ({ label, icon, ...other}) => {
  return (
    <Wrapper hasLabel={!!label}>
      {!!label &&
        <Label>{label}</Label>
      }
      <Input type="text" icon={icon} {...other} />
      {!!icon &&
        <Icon className="material-icons">
          {icon}
        </Icon>
      }
    </Wrapper>
  )
};

TextField.propTypes = {
  icon: PropTypes.node,
  label: PropTypes.string,
};

export default TextField;
