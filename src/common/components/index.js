import styled from 'styled-components';
import { colors } from '../containers/App/GlobalStyles';

export const H3 = styled.h3`
  z-index: 100;
  margin: 0;
  font-size: 26px;
  font-weight: 500;
  line-height: 38px;
  color: ${colors.textColor};

  strong {
    font-weight: 700;
  }
`;

export const WhiteH3 = styled(H3)`
  color: rgba(255,255,255,0.7);

  strong {
    color: #fff;
  }
`;

const Button = styled.button`
  position: relative;
  padding: 10px 20px;
  display: inline-block;
  background-color: ${colors.primaryColor};
  transition: all 0.2s ease-in-out;
  overflow: hidden;
  border: none;
  border-radius: 4px;
  box-shadow: 0 4px 12px rgba(102,103,107,0.15);
  cursor: pointer;
  line-height: 24px;
  color: #fff;
  font-size: 16px;
  font-weight: 500;
`;

export const BigButton = styled(Button)`
  height: 52px;
  padding: 0 30px;
  font-size: 18px;
  line-height: 52px;
`;

