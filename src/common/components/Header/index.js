import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Navbar from './Navbar';
import belezapLogo from './logo-belezap.png';
import belezapWhiteLogo from './logo-belezap-branca.png';
import { breakPoints } from '../../containers/App/GlobalStyles';

const HeaderContainer = styled.header`
  display: flex;
  z-index: 10000;
  position: absolute;
  top: 0;
  width: 100%;
  height: 76px;
  ${({ transparentBg }) => !transparentBg && `
    box-shadow: 0 0 18px 0 rgba(0, 0, 0, 0.12);
  `}
  ${({ transparentBg }) => transparentBg && `
    border-bottom: 1px solid rgba(255,255,255,0.2);
  `}
  font-size: 16px;
  background-color: ${({transparentBg}) => (transparentBg
    ? 'transparent'
    : '#fff')};

  @media (min-width: ${breakPoints.tablet}) {
    height: 82px;
  }
`;

const Logo = styled.div`
  display: flex;
  padding: 0 20px;
  @media (min-width: ${breakPoints.tablet}) {
    padding: 0 35px;
    border-right: 1px solid #e0e0e0;
    ${({ whiteLogo }) => whiteLogo && `
      border-color: rgba(255,255,255,0.2);
    `}
  }
  h1 {
    display: flex;
    align-items: center;
    margin: 0;
    a {
      display: block;
      width: 100px;
      height: 28px;
      background-image: url(
          ${({whiteLogo}) => (whiteLogo
            ? belezapWhiteLogo
            : belezapLogo)}
        );
      background-size: cover;
      background-repeat: no-repeat;
      text-indent: -9999px;
      font-size: 0;
      @media (min-width: ${breakPoints.tablet}) {
        width: 150px;
        height: 42px;
      }
    }
  }
`;

const Header = ({ isBgTransparent }) => (
  <HeaderContainer transparentBg={isBgTransparent}>
    <Logo whiteLogo={isBgTransparent}>
      <h1><a href="/" title="Belezap">Belezap</a></h1>
    </Logo>
    <Navbar whiteText={isBgTransparent} />
  </HeaderContainer>
);

Header.propTypes = {
  isBgTransparent: PropTypes.bool,
};

export default Header;
