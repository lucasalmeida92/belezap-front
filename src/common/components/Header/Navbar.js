import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { colors, breakPoints } from '../../containers/App/GlobalStyles';

const NavbarContainer = styled.ul`
  display: none;

  @media (min-width: ${breakPoints.tablet}) {
    flex: 1;
    display: flex;
    align-items: center;
    padding: 0 35px;
  }
`;

const Li = styled.li`
  padding: 0 15px;
  margin: 0 5px;
  a {
    color: ${({whiteText}) => (whiteText
      ? '#fff'
      : '#666')};
    &:hover {
      color: ${({whiteText}) => (whiteText
        ? '#fff'
        : colors.primaryColor)};
    }
  }
`;

const Navbar = ({ whiteText }) => (
  <NavbarContainer>
    <Li whiteText={whiteText}>
      <Link to="/seja-um-belezap">Seja um Belezap</Link>
    </Li>
    <Li whiteText={whiteText}>
      <Link to="/como-funciona">Como funciona</Link>
    </Li>
    <Li whiteText={whiteText}>
      <Link to="/blog">Blog</Link>
    </Li>
    <Li whiteText={whiteText}>
      <Link to="/entrar">Entrar</Link>
    </Li>
  </NavbarContainer>
);

Navbar.propTypes = {
  whiteText: PropTypes.bool,
};

export default Navbar;
