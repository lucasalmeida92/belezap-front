import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  margin: 0 auto;
  padding: 0 16px;

  @media(min-width: 576px) {
    max-width: 540px;
  }
  @media(min-width: 768px) {
    max-width: 720px;
  }
  @media(min-width: 992px) {
    max-width: 960px;
  }
  @media(min-width: 1240px) {
    max-width: 1090px;
  }
  @media(min-width: 1367px) {
    max-width: 1210px;
  }
`;

const ResponsiveContainer = ({ children }) => (
  <Container>
    { children }
  </Container>
);

ResponsiveContainer.propTypes = {
  children: PropTypes.node,
};

export default ResponsiveContainer;
