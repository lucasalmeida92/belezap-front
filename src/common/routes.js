import Loadable from 'react-loadable';

const loadComponent = (path) => Loadable({
  loader: () => import(`${path}`),
  loading: () => null,
});

export default [
  {
    component: loadComponent('./containers/App'),
    routes: [
      {
        path: '/',
        exact: true,
        component: loadComponent('./containers/HomePage'),
      },
      {
        path: '/posts',
        component: loadComponent('./containers/PostsPage'),
      },
      {
        component: loadComponent('./components/NotFound'),
      },
    ],
  },
];
