import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { renderRoutes as renderSubRoutes } from 'react-router-config';
import styled, { ThemeProvider } from 'styled-components';
import GlobalStyles, { theme } from './GlobalStyles';
import Header from '../../components/Header';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 82px;
`;

const PageContent = styled.div`
  flex: 1;
`;

const App = props => {
  const isBgTransparent = props.location.pathname === '/';

  return (
    <React.Fragment>
      <Helmet
        title="Conectando bem estar!"
        titleTemplate="Belezap - %s"
        link={[
          {
            href: 'https://fonts.googleapis.com/css?family=Nunito:300,400,600,700',
            rel: 'stylesheet',
          },
        ]}
        meta={[
          { charset: 'utf-8' },
          {
            'http-equiv': 'X-UA-Compatible',
            content: 'IE=edge',
          },
          {
            name: 'viewport',
            content: 'width=device-width, initial-scale=1, maximum-scale=1',
          },
        ]} />
      <GlobalStyles />
      <ThemeProvider theme={theme}>
        <Container>
          <Header isBgTransparent={isBgTransparent}/>
          <PageContent>
            { renderSubRoutes(props.route.routes) }
          </PageContent>
        </Container>
      </ThemeProvider>
    </React.Fragment>
  );
};
// SSR
// Actions required for Client/Server to provide didMount data to this component.
// Must return array. See more at common/preload/Dataloader.fetchData.
App.need = ({ dispatch }, { params }) => [ // eslint-disable-line
  console.log('need: First dispatch from App with params:', params),
  console.log('need: Another dispatch from App with params:', params),
];

App.propTypes = {
  route: PropTypes.object,
  location: PropTypes.object,
};

export default App;
