import { createGlobalStyle } from 'styled-components';
import normalizeCss from './normalize';

export const colors = {
  textColor: '#333333',
  primaryColor: '#9C27B0',
};

export const breakPoints = {
  phone: '576px',
  tablet: '768px',
  tabletLarge: '992px',
  desktop: '1240px',
  desktopLarge: '1367px',
};

export const theme = {
  breakpoints: [
    breakPoints.phone,
    breakPoints.tablet,
    breakPoints.tabletLarge,
    breakPoints.desktop,
    breakPoints.desktopLarge
  ],
  colors,
};

const GlobalStyles = createGlobalStyle`
  ${normalizeCss}

  /* @font-face {
    font-family: 'Material Icons';
    font-style: normal;
    font-weight: 400;
    src: url(https://example.com/MaterialIcons-Regular.eot);
    src: local('Material Icons'),
      local('MaterialIcons-Regular'),
      url(https://example.com/MaterialIcons-Regular.woff2) format('woff2'),
      url(https://example.com/MaterialIcons-Regular.woff) format('woff'),
      url(https://example.com/MaterialIcons-Regular.ttf) format('truetype');
  } */

  body {
    color: ${colors.textColor};
    font-family: 'Nunito', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  a {
    text-decoration: none;
    color: ${colors.textColor};
    transition: color 0.25s;

    &:hover {
      color: ${colors.primaryColor};
    }
  }

  ul {
    list-style: none;
    padding: 0;
    margin: 0;
  }
`;

export default GlobalStyles;
