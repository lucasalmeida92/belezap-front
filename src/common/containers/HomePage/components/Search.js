import React from 'react';
import styled from 'styled-components';
import { breakPoints } from '../../App/GlobalStyles';
import searchBackground from './search-bg.jpg';
import TextField from '../../../components/TextField';
import { WhiteH3, BigButton } from '../../../components';
import LocationIcon from '@material-ui/icons/LocationOnOutlined';
import CalendarIcon from '@material-ui/icons/CalendarTodayOutlined';

const SearchContainer = styled.div`
  box-sizing: border-box;
  position: relative;
  min-height: 100vh;
  padding-top: 76px;
  margin-top: -82px;

  @media (min-width: ${breakPoints.tablet}) {
    padding: 82px 0;
    margin-top: -82px;
  }

  &:after {
    content: '';
    position: absolute;
    height: 100%;
    width: 100%;
    display: block;
    top: 0;
    left: 0;
    z-index: 15;
    background: #333;
    opacity: 0.75;
  }
`;

const BgImage = styled.div`
  z-index: 10;
  overflow: hidden;
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  background-image: url(${searchBackground});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  z-index: 100;
  position: relative;
  box-sizing: border-box;
  width: 100%;
  padding: 64px 15px 70px;
`;

class Search extends React.Component {
  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
  }

  render() {
    return(
      <SearchContainer>
        <Content>
          <WhiteH3 style={{fontSize: '24px', fontWeight: '300' }}>
            <strong>O Belezap que você deseja</strong>
            <br/>
            <span>No local e quando quiser, sem trânsito e sem espera!</span>
          </WhiteH3>
          <form onSubmit={this.handleSubmit}>
            <TextField
              big
              name="location"
              label="Local"
              placeholder="Em que bairro você está?"
              icon={<LocationIcon />}
              onChange={this.handleInputChange} />
            <TextField
              big
              name="date"
              label="Quando?"
              placeholder="28/11/2018"
              icon={<CalendarIcon />}
              onChange={this.handleInputChange} />
            <TextField
              big
              name="time"
              placeholder="Horário"
              onChange={this.handleInputChange} />
            <BigButton type="submit">Encontrar Belezap</BigButton>
          </form>
        </Content>
        <BgImage />
      </SearchContainer>
    )
  }
};

export default Search;
