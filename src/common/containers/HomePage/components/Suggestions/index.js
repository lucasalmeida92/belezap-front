import React from 'react';
import styled from 'styled-components';
import { H3 } from '../../../../components';
import ResponsiveContainer from '../../../../components/ResponsiveContainer';
import { Flex, Box } from '@rebass/grid';
import CardLink from './CardLink';

import nailsImage from './assets/nails.png';
import femaleDepilationImage from './assets/female-depilation.png';
import maleDepilationImage from './assets/male-depilation.png';
import massageImage from './assets/massage.png';
import femaleHairImage from './assets/female-hair.png';
import maleHairImage from './assets/male-hair.png';
import beardImage from './assets/beard.png';
import comboImage from './assets/combo.png';

const Section = styled.div`
  box-sizing: border-box;
  padding: 65px 0;
  background-color: #fff;
  text-align: center;
`;

const Suggestions = () => {
  return(
    <Section>
      <ResponsiveContainer>
        <H3 style={{marginBottom: '45px'}}>Dá uma olhada <strong>nas nossas sugestões:</strong></H3>
        <Flex flexWrap="wrap">
          <Box width={[ 1, 1, 1/2, 1/2, 1/4]} px={3}>
            <CardLink bgImage={nailsImage} href="#" text="Unhas" />
          </Box>
          <Box width={[ 1, 1, 1/2, 1/2, 1/4]} px={3}>
            <CardLink bgImage={femaleDepilationImage} href="#" text="Depilação Feminina" />
          </Box>
          <Box width={[ 1, 1, 1/2, 1/2, 1/4]} px={3}>
            <CardLink bgImage={maleDepilationImage} href="#" text="Depilação Masculina" />
          </Box>
          <Box width={[ 1, 1, 1/2, 1/2, 1/4]} px={3}>
            <CardLink bgImage={massageImage} href="#" text="Massagem" />
          </Box>
          <Box width={[ 1, 1, 1/2, 1/2, 1/4]} px={3}>
            <CardLink bgImage={femaleHairImage} href="#" text="Cabelo Feminino" />
          </Box>
          <Box width={[ 1, 1, 1/2, 1/2, 1/4]} px={3}>
            <CardLink bgImage={maleHairImage} href="#" text="Cabelo Masculino" />
          </Box>
          <Box width={[ 1, 1, 1/2, 1/2, 1/4]} px={3}>
            <CardLink bgImage={beardImage} href="#" text="Barba" />
          </Box>
          <Box width={[ 1, 1, 1/2, 1/2, 1/4]} px={3}>
            <CardLink bgImage={comboImage} href="#" text="Combos" />
          </Box>
        </Flex>
      </ResponsiveContainer>
    </Section>
  )
};

export default Suggestions;
