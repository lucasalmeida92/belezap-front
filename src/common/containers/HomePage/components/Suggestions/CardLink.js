import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from '../../../../containers/App/GlobalStyles';

const Container = styled.a`
  position: relative;
  margin: 0 0 30px 0;
  border-radius: 4px;
  height: 160px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-size: cover;
  background-position: 100%;
  box-shadow: 0 3px 10px rgba(0,0,0,0.2);
  transition: 0.4s;
  color: #fff;
  font-weight: 600;
  ${({ bgImage }) => bgImage && `
    background-image: url(${bgImage});
  `}

  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: #333;
    opacity: 0.5;
    border-radius: 4px;
    transition: 0.4s;
  }

  &:hover {
    transform: translateY(-5px);
    box-shadow: 0 4px 12px rgba(42,65,232,0.25);

    &:before {
      opacity: 0.95;
      background: ${colors.primaryColor};
    }
  }
`;

const Text = styled.h4`
  position: absolute;
  top: 50%;
  right: 0;
  bottom: auto;
  left: 0;
  margin: 0 auto;
  transform: translateY(-49%);
  text-align: center;
  color: #fff;
`;

const CardLink = ({ bgImage, text, ...other }) => {
  return(
    <Container bgImage={bgImage} {...other}>
      <Text>{ text }</Text>
    </Container>
  )
};

CardLink.propTypes = {
  bgImage: PropTypes.string,
  text: PropTypes.string,
};

export default CardLink;
