import React, { Component } from 'react';
import Search from './components/Search';
import Suggestions from './components/Suggestions';

class HomePage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <Search />
        <Suggestions />
      </React.Fragment>
    );
  }
}

export default HomePage;
