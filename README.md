Belezap Front-end
=====================

The Front-end of Belezap website.


# Usage
```
# Development
yarn install
yarn start
open http://localhost:3000

# Production
yarn install
yarn production
open http://localhost:5000

```

# Dependencies
* Webpack
* React + Redux
* [Razzle Universal Webpack Lib](https://github.com/jaredpalmer/razzle/tree/master/packages/razzle)
* [react-router](https://github.com/ReactTraining/react-router)
* [styled-components](https://github.com/styled-components/styled-components)
* [react-helmet](https://github.com/nfl/react-helmet) - A document head manager for React

# Plus
* Winston + exception logger

# Patterns / LINT
* [ESLINT](https://github.com/eslint/eslint)

# TODOS:
* Remove Redux-tunk and use Rematch(redux)
* Components Documentation(Storybook or Docz)
* Unit Tests setup
* Integration Tests setup
* Mock-API
